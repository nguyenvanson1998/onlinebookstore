<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define('DB_NAME', 'n.son44');

/** Username của database */
define('DB_USER', 'n.son44');

/** Mật khẩu của database */
define('DB_PASSWORD', '2421998');

/** Hostname của database */
define('DB_HOST', 'localhost');

/** Database charset sử dụng để tạo bảng database. */
define('DB_CHARSET', 'utf8mb4');

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@SK|^gv{`{*8>Anr}v)@z%<sd1a}5`nv0`b0@3Rynjybi6!``GU*72Dsv_VoeP,]');
define('SECURE_AUTH_KEY',  '}=$,#^|uN< :@&b#Tn!s<R4IVuTH[wEiVWtA_[328w,IUA5<[$ =n>z#2?fmqAlg');
define('LOGGED_IN_KEY',    'E{0M3[nj9dPfUHR$+X9o_Ydp/%BT>Y 4-Pg!~S<A.6YO=P<qsj4u#zr$_GSDnP$_');
define('NONCE_KEY',        'mR@Tn!W;c8uGJrBn>Mm 0I^#V d6#Cv8Vy&[vFvIdL*p~g/$JHC`27 Z&OlrG^zL');
define('AUTH_SALT',        'j$hDVhZS))?M$Z:(V>ac`r0G/|eh;?<,7,sA)d3FL{W?MhGb3z7j5S +MA!KFK1?');
define('SECURE_AUTH_SALT', 'S&]<By-yn9X[~>y=MI>PqQM$H:LD=rohv&(oHmtGPP&o()^Wn#*%i2CgXl%<x[Uf');
define('LOGGED_IN_SALT',   '&8UCmc|muB6BP!JW /WqB@ra{&$[}D+SXrT8!2.ja]ZCn/]a1&zQu3^$n7Z}CZjL');
define('NONCE_SALT',       '.R6smt4w^c%n4,&f2&MGEZIEA{dMLSwSJ&7B+J^>QXEb3T`<HS>Uq}]&X#jrHvIM');

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'son_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
